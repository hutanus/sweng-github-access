# GitHub API access demo #

This Node.js app provides a simple interface to make two types of requests to the GitHub API. The app takes in a username and "List repos" or "User info" and makes the appropriate API calls to `api.github.com`.

The app uses [Material UI](https://material-ui.com/) for the user interface and runs on Node 14.15.3 (LTS at the time of writing, based on the `node` container with the `lts` tag on [Docker Hub](https://hub.docker.com/_/node/)).[

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

If you're on macOS or Linux, you can run the bash script `run.sh` to build and run the docker container. If you're on Windows, open the script and run the commands manually.

![App screenshot](app_screenshot.png)